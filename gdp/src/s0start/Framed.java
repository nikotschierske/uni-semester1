package s0start;

import java.awt.Color;

import std.ui.MiniApp;

public class Framed extends MiniApp {
	
	public static int DIAMETER = 10;
	
	public static void main(String[] args) {
		char lastKey = 'p';
		int posX = WINDOW_WIDTH / 2;
		int posY = WINDOW_HEIGHT / 2;
		fillCircle("ball", posX, posY, DIAMETER, Color.black);
		while(lastKey != ESC_KEY) {
			switch (lastKey) {
			case 'w':
				if ((posY - 2) >= (DIAMETER)) {
					posY -= 2;
				}
				break;
			case 's':
				if ((posY + 2) <= (WINDOW_HEIGHT - DIAMETER)) {
					posY += 2;
				}
				break;
			case 'a':
				if ((posX - 2) >= (DIAMETER)) {
					posX -= 2;
				}
				break;
			case 'd':
				if ((posX + 2) <= (WINDOW_WIDTH - DIAMETER)) {
					posX += 2;
				}
				break;

			default:
				break;
			}
			
			removeDrawable("ball");
			fillCircle("ball", posX, posY, 10, Color.black);
			lastKey = getKey();
		}
	}
}
