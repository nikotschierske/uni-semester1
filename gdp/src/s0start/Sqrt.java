package s0start;

import java.util.Scanner;

public class Sqrt {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double a = scanner.nextDouble();
		scanner.close();
		if (a > 0) {
			double d, z, x;
			z = x = d = 1;
			x = a/2;
			while (d > 0.000001) {
				z = ((a/x) + x) / 2 ;
				d = (z-x) * (z-x);
				x = z; 
			} 
			System.out.println("sqrt = " + z);
		}
		else {
			System.out.println("sqrt = NaN"); //Error first, nach oben
		}
	}
}
