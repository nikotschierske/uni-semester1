package s6array;

public class DebugMe5 {
	
	/**
	 * Sortiert die Zahlen a bis d in aufsteigender Reihenfolge.
	 * @param a - 1. Zahl: int.
	 * @param b - 2. Zahl: int.
	 * @param c - 3. Zahl: int.
	 * @param d - 4. Zahl: int.	
	 */
	public static void sort(int a, int b, int c, int d) {
		int t;
		t = a; a = b; b = t;
		if (c > d) {
			t = c; c = d; d = t;
		}
		if (b > c) {
			t = b; b = c; c = t;
		}
		if (a > b) {
			t = a; a = b; b = t;
		}
		//------------------------------------
		if (c > d) {
			t = c; c = d; d = t;
		}
		if (b > c) {
			t = b; b = c; c = t;
		}
		//-------------------------------------	
		if (c > d) {
			t = c; c = d; d = t;
		}
		
		
		System.out.printf("%d, %d, %d, %d\n", a, b, c, d);
	}
	
	public static void main(String[] args) {
		sort(1, 2, 3, 4);
		sort(1, 2, 4, 3);
		sort(1, 3, 4, 2);
		sort(1, 3, 2, 4);
		sort(2, 3, 4, 1);
		sort(3, 2, 1, 4);
		sort(4, 3, 2, 1);
		sort(3, 2, 1, 4);
		sort(4, 2, 1, 3);
		sort(2, 4, 3, 1);
	}

}
