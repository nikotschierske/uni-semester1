 package s0start;
 
 import std.ui.MiniApp;
 
 public class Steinpfad extends MiniApp {
 	public static void main(String[] args) {
 		
 		//Basis
 		placeBrick(0, true, 2);
 		placeBrick(6, true, 3);
 		placeBrick(23, true, 2);
 		placeBrick(26, true, 4);
 		
 		//Brückenteile
 		placeBrick(0, true, 8);
 		placeBrick(8, true, 8);
 		placeBrick(23, false, 8);
 		placeBrick(24, true, 6);
 	}
 } 