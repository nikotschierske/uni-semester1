package s0start;

import java.util.Scanner;

public class SimpleStat {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int lastInput = scanner.nextInt();
		int i = 0;
		
//		int input0 = 0;
//		int input1 = 0;
//		int input2 = 0;
//		int input3 = 0;
//		while (lastInput != 0) {
//			if (input0 == 0){
//				input0 = lastInput;
//			} else if (input1 == 0) {
//				input1 = lastInput;
//			} else if (input2 == 0) {
//				input2 = lastInput;
//			} else if (input3 == 0) {
//				input3 = lastInput;
//			}
//		}
		
		int[] inputInts = new int[4];
		while (lastInput != 0) {
			inputInts[i] = lastInput;
			i++;
			lastInput = scanner.nextInt();
		}
		double sum = 0;
		boolean allsame = true, increasing = true;
		int min = 10000000;
		int max = 0;
		int countActualNs = 0;
		
		for (int j = 0; j < inputInts.length; j++) {
			int currInput = inputInts[j];
			if (currInput != 0) {
				countActualNs++;
				sum += currInput;
				if (min > currInput) { min = currInput; }
				if (max < currInput) { max = currInput; }
				if (j != 0 && inputInts.length != 1) {
					allsame = allsame && (currInput == inputInts[j-1]);
					increasing = increasing && (currInput > inputInts[j-1]);
				}
			}
		}
		System.out.println("min = " + min);
		System.out.println("max = " + max);
		System.out.println("avg = " + (double) sum/countActualNs);
		if (increasing) { System.out.println("increasing"); }
		else if (allsame) { System.out.println("allsame"); }
		scanner.close();
	}
}
