package s0start;

import java.util.Scanner;

public class Kniffel {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int throw1 = scanner.nextInt();
		int throw2 = scanner.nextInt();
		int throw3 = scanner.nextInt();
		int throw4 = scanner.nextInt();
		int throw5 = scanner.nextInt();
		
		
		boolean kniffel, pasch, gerade, ungerade, fullHouse; 
		int sameN, summe; 
		kniffel = gerade = ungerade = true; pasch = fullHouse = false;
		sameN = summe = 0; 
		
		summe = throw1 + throw2 + throw3 + throw4 + throw5;
		kniffel = (throw1 == throw2) && (throw2 == throw3) && (throw3 == throw4) && (throw4 == throw5);
		gerade = (throw1%2==0) && (throw2%2==0)&& (throw3%2==0)&& (throw4%2==0)&& (throw5%2==0);
		ungerade = (throw1%2==1) && (throw2%2==1)&& (throw3%2==1)&& (throw4%2==1)&& (throw5%2==1);

		if ((throw1 == throw2) || (throw1 == throw3) || (throw1 == throw4) || (throw1 == throw5))
			sameN++;
		if ((throw2 == throw3) || (throw2 == throw4) || (throw2 == throw5))
			sameN++;
		if ((throw3 == throw4) || throw3 == throw5)
			sameN++;
		if (throw4 == throw5)
			sameN++;
		
		System.out.println(sameN);
		pasch = sameN > 0; //Zählen sich zwei Zahlen mehr als Null mal gegenseitig als gleiche, liegt eine Zahl mehr als ein Mal vor --> Pasch
		fullHouse = (sameN == 3) && !kniffel; //Kommt ein FullHouse vor, werden insgesamt 3 gleiche Nummern gezählt
		
		System.out.println(sameN);
		System.out.println("Summe = " + summe);
		System.out.println("Kniffel = " + kniffel);
		System.out.println("Pasch  = " + pasch);
		System.out.println("Gerade = " + gerade);
		System.out.println("Ungerade = " + ungerade);
		System.out.println("FullHouse = " + fullHouse);
		scanner.close();
	}
	
// KNIFFEL MIT ARRAY UND LOOP	
// 
//	public static void main(String[] args) {
//		Scanner scanner = new Scanner(System.in);
//		int[] dieThrows = {
//				scanner.nextInt(),
//				scanner.nextInt(),
//				scanner.nextInt(),
//				scanner.nextInt(),
//				scanner.nextInt()
//		};
//		scanner.close();
//		
//		boolean kniffel, pasch, gerade, ungerade, fullHouse; 
//		int sameN, summe; 
//		kniffel = gerade = ungerade = true; pasch = fullHouse = false;
//		sameN = summe = 0;
//		
//		
//		for (int i = 0; i < dieThrows.length; i++) {
//			//Doppelte Iteration für's Zählen von gleichen Zahlen
//			for (int j = 0; j < dieThrows.length; j++) {
//				if (dieThrows[i]==dieThrows[j] && !(i == j))
//					sameN++;
//			}
//			
//			summe += dieThrows[i];
//			gerade = gerade && (dieThrows[i]%2==0);
//			ungerade = ungerade && (dieThrows[i]%2==1);
//			
//			if (i!=0)
//				kniffel = kniffel && (dieThrows[i] == dieThrows[i-1]);
////			System.out.println(dieThrows[i]);
//		}
//		pasch = sameN > 0; //Zählen sich zwei Zahlen mehr als Null mal gegenseitig als gleiche, liegt eine Zahl mehr als ein Mal vor --> Pasch
//		fullHouse = (sameN == 8) && !kniffel; //Kommt ein FullHouse vor, zählen sich die gleichen Zahlen im Loop insgesamt 8 mal
//		
//		System.out.println(sameN);
//		System.out.println("Summe = " + summe);
//		System.out.println("Kniffel = " + kniffel);
//		System.out.println("Pasch  = " + pasch);
//		System.out.println("Gerade = " + gerade);
//		System.out.println("Ungerade = " + ungerade);
//		System.out.println("FullHouse = " + fullHouse);
//	}
}
