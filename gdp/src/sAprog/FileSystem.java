package sAprog;

import java.util.Arrays;

public class FileSystem {
    public static void main(String[] args) {
        File f = new File("test", 33);
        System.out.println(f.toString());
    }
}

class File {
    private String name;
    private long size;

    public File(String name, long size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public long getSizeInBytes() {
        return size;
    }

    public String toString() {
        return String.format("%s (%d Byte)", this.name, this.size);
    }
}

class Folder {
    private String name;
    private File[] files;
    private Folder[] folders;
    private int docCount = 0;
    private int imageCount = 0;

    public Folder(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void add(File... addFiles) {
        for (int i = 0; i < addFiles.length; i++) {
            add(addFiles[i]);
        }
    }

    public void add(File addFile) {
        boolean replaced = false;
        for (int i = 0; i < this.files.length; i++) {
            if (addFile.getName() == this.files[i].getName()){
                this.files[i] = addFile;
                replaced = true;
            }

            if (isDocument(addFile)) {
                this.docCount++;
            }
            if (isImage(addFile)) {
                this.imageCount++;
            }
        }
        if (!replaced) {
            this.files = Arrays.copyOf(this.files, this.files.length + 1);
        }
    }

    public void add(Folder[] addFolders) {
        for (int i = 0; i < addFolders.length; i++) {
            add(addFolders[i]);
        }
    }

    public void add(Folder addFolder) {
        boolean replaced = false;
        for (int i = 0; i < this.folders.length; i++) {
            if (addFolder.getName() == this.folders[i].getName()){
                this.folders[i].add(addFolder.getFiles());
                this.folders[i].add(addFolder.getFolders());
                replaced = true;
            }
        }
        if (!replaced) {
            this.folders = Arrays.copyOf(this.folders, this.folders.length + 1);
        }
    }

    private File[] getFiles() {
        return this.files;
    }

    private Folder[] getFolders() {
        return this.folders;
    }

    public File[] findDocuments() {
        File[] docs = new File[docCount];
        int docsI = 0;
        for (int i = 0; i < this.files.length; i++) {
            if (isDocument(this.files[i])) {
                docs[docsI] = this.files[i];
                docsI++;
            }
        }
        return docs;
    }

    public File[] findImages() {
        File[] imgs = new File[imageCount];
        int imgI = 0;
        for (int i = 0; i < this.files.length; i++) {
            if (isImage(this.files[i])) {
                imgs[imgI] = this.files[i];
                imgI++;
            }
        }
        return imgs;
    }

    private boolean isDocument(File file) {
        boolean isDoc = false;
        isDoc |= file.getName().contains(".doc");
        isDoc |= file.getName().contains(".txt");
        isDoc |= file.getName().contains(".pdf");
        return isDoc;
    }

    private boolean isImage (File file) {
        boolean isImage = false;
        isImage |= file.getName().contains(".jpg");
        isImage |= file.getName().contains(".jpeg");
        isImage |= file.getName().contains(".png");
        return isImage;
    }
}