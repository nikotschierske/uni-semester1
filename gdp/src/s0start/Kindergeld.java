package s0start;

import java.util.Scanner;

public class Kindergeld {
	static int PRO_KIND_U3 = 219;
	static int PRO_KIND_U4 = 225;
	static int PRO_KIND_DRÜBER = 250;
	 
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int anzahlKinder = scanner.nextInt();
		scanner.close();
		int anspruch = 0;
		if (anzahlKinder > 0) {
			if (anzahlKinder > 3) {
				anspruch += (anzahlKinder - 3) * PRO_KIND_DRÜBER;
			}
			if (anzahlKinder > 2) {
				anspruch += PRO_KIND_U4;
			}
			if (anzahlKinder == 1) {
				anspruch = PRO_KIND_U3;
			}
			else {
				anspruch += 2*PRO_KIND_U3;
			}
		}
		System.out.println("Anspruch = " + anspruch);
	}
}
