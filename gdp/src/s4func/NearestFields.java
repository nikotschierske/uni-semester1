package s4func;

import java.util.Scanner;

public class NearestFields {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int x = scanner.nextInt();
		int y = scanner.nextInt();
		int distance = scanner.nextInt();
		int absDistance = Math.abs(distance);

		int maxX = x + absDistance;
		int minX = x - absDistance;
		int maxY = y + absDistance;
		int minY = y - absDistance;

		for (int i = 0; i < absDistance + 1; i++) {
			for (int xi = minX; xi <= maxX; xi++) {
				for (int yi = minY; yi <= maxY; yi++) {
					int dx = Math.abs(x-xi); 
					int dy = Math.abs(y-yi);
					int thisDistance = (dx > dy) ? dx : dy;
					if (thisDistance == i) {
						System.out.println(xi + ", " + yi );
					}
				}
			}
		}
		scanner.close();
	}
}
