package s8mthd;

import java.util.Arrays;

public class CardStacks {

	public static void main(String[] args) {
		CardDeck deck = new CardDeck(3);
		System.out.println(deck.cards.length);
		System.out.println(deck.cards[0]);
		CardDeck deck2 = new CardDeck(3);
		deck.moveAll(deck2);
		System.out.println(deck2.cards.length);
		System.out.println(deck2.cards[4]);
	}
}

class Card {
	private int value;

	public Card() {
		this.value = (int) (Math.random() * 8 + 2);
	}

	public Card(int value) {
		this.value = value;
	}

	public int getNum() {
		return value;
	}
}

class CardDeck {
	public Card[] cards; //change

	public CardDeck(int numberOfCards) {
		this.cards = new Card[numberOfCards];
		
		for (int i = 0; i < this.cards.length; i++) {
			this.cards[i] = new Card();
		}
	}

	public Card getTop() {
		if (isEmpty()) {
			return null;
		}
		return this.cards[this.cards.length - 1];
	}

	public void moveTop(CardDeck cd) {
		Card cardToMove = removeTopCard();
		cd.addCardToTop(cardToMove);
	}

	private Card removeTopCard() {
		Card toReturn = this.getTop();
		
		if (!isEmpty()) {
			this.cards = Arrays.copyOf(this.cards, this.cards.length - 1);
		}
		return toReturn;
	}

	public void addCardsToTop(Card[] newCards) {
		int newLength = this.cards.length + newCards.length;
		int oldLength = this.cards.length;
		
		this.cards = Arrays.copyOf(this.cards, newLength);
		
		for (int i = oldLength; i < newLength; i++) {
			this.cards[i] = newCards[i - oldLength];
		}
	}

	public void addCardToTop(Card cardToAdd) {
		if (cardToAdd != null) {
			this.cards = Arrays.copyOf(this.cards, this.cards.length + 1);
			this.cards[this.cards.length - 1] = cardToAdd;
		}
	}

	public void moveAll(CardDeck cardDeckTo) {
		cardDeckTo.addCardsToTop(this.cards);
		this.cards = new Card[0];
	}

	public boolean isEmpty() {
		return this.cards.length == 0;
	}

	public boolean isSingle() {
		return this.cards.length == 1;
	}
}