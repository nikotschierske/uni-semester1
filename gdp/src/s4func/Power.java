package s4func;

public class Power {
	public static double power(double x, int n) {
	 	if (n == 0) {
	 		return 1.0;
	 	}
	 	double z = x;
	 	while (n > 1) {
	 		z *= z;
	 		n /= 2.0;
	 	}
	 	return z;
	 }
}
