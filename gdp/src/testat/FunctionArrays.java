package testat;

public class FunctionArrays {

	public static long FACTOR = 100L;
	public static void main(String[] args) {
		long[] werte = {0, 600, 12300 ,24512 , -3345};
		long[][] table = {
				{3245, -1234, 2654, 0},
				{25, 7, -2343251, 2323},
				{-34352, 526, 4, -4}
		};
		
		long[][] tableMissingLine = {
				{3245, -1234, 2654, 0},
				{}, 					//Ist das mit fehlender Zeile gemeint?
				{-34352, 526, 4, -4}
		};
		for (int i = 0; i < werte.length; i++) {
			System.out.println(isEvenNumber(werte[i]));
		}
		System.out.println(countEvenNumbers(werte));
		
		System.out.println(countEvenNumbers(table[0]));
		System.out.println("Even Numbers per line of table:");
		int[] evenNsInTable = evenNumbersInMatrix(tableMissingLine);
		for (int i = 0; i < evenNsInTable.length; i++) {
			System.out.println(evenNsInTable[i]);
		}
		System.out.println("----");
		
		System.out.println("Filtered List, only numbers multiple of FACTOR:");
		long[] filtered = filter(werte);
		
		for (int i = 0; i < filtered.length; i++) {
			System.out.println(filtered[i]);
		}
		System.out.println("----");
		
		//Edgecase testing
		System.out.println(countEvenNumbers(new long[0]));
	}
	
	/**
	 * Tests if a long is an even number. 
	 * Works for negative and positive longs.
	 * Input of 0 returns true;
	 * @param toCheck input number
	 * @return true if even number (or 0) was the input
	 */
	public static boolean isEvenNumber(long toCheck) {
		return (toCheck % 2 == 0);
	}
	
	/**
	 * Counts even numbers in an array
	 * @param toCheck
	 * @return number of even numbers in the array
	 */
	public static int countEvenNumbers(long[] toCheck) {
		int counter = 0;
		for (int i = 0; i < toCheck.length; i++) {
			if (isEvenNumber(toCheck[i])) {
				counter++;
			}
		}
		return counter;
	}
	
	/**
	 * Counts even numbers in a long-matrix.
	 * @param matrix to be counted
	 * @return array of even numbers count by line of matrix
	 */
	public static int[] evenNumbersInMatrix(long[][] matrix) {
		int[] evenNumberCounts = new int[matrix.length];
		for (int i = 0; i < evenNumberCounts.length; i++) {
			if (matrix[i].length == 0) {
				evenNumberCounts[i] = -1;
			} else {
				evenNumberCounts[i] = countEvenNumbers(matrix[i]);
			}
		}
		return evenNumberCounts;
	}
	
	/**
	 * Returns an array of longs, that are in toFilter and divisible by the constant FACTOR
	 * @param toFilter Array that is supposed to be filtered
	 * @return Array of longs that are divisible by FACTOR
	 */
	public static long[] filter(long[] toFilter) {
		int counter = 0;
		for (int i = 0; i < toFilter.length; i++) {
			if (toFilter[i] % FACTOR == 0) {
				counter++;
			}
		}
		long[] filteredList = new long[counter];
		int filteredI = 0;
		for (int i = 0; i < toFilter.length; i++) {
			if (toFilter[i] % FACTOR == 0) { //Gilt 0 als vielfaches von allen reellen Zahlen? Wenn nicht soltle hier (toFilter[i] % FACTOR == 0 && toFilter[i] != 0) stehen  
				filteredList[filteredI] = toFilter[i];
				filteredI++;
			}
		}
		return filteredList;
	}
}
