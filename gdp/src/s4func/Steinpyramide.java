package s4func;

import java.util.Scanner;

import std.ui.MiniApp;

public class Steinpyramide extends MiniApp {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int p = scanner.nextInt(); //Startposition
		int h = scanner.nextInt(); //Höhe
		int maxWidth = h*2 - 1; 
		
		for (int i = 0; i < h; i++) { //für jede Reihe (Höhe)
			int width = maxWidth - i*2;
			int[] stones = splitToStones(width);
			int position = p + i;
			for (int j = 0; j < stones.length; j++) {				
				for (int j2 = 0; j2 < stones[j]; j2++) {
					placeBrick(position, true, j);
					position += j;
				}
			}
		} 
		scanner.close(); 
	}
	
	public static int[] splitToStones(int x) {
		//Mögliche Steine: 1, 2, 3, 4, 6 und 8
		int[] rArray = new int[9];
		rArray[8] = x / 8;
		x = x % 8;
		rArray[6] = x / 6;
		x = x % 6;
		rArray[4] = x / 4;
		x = x % 4;
		rArray[3] = x / 3;
		x = x % 3;
		rArray[2] = x / 2;
		x = x % 2;
		rArray[1] = x / 1;
		x = x % 1;
		return rArray;
	}
}
