package sAprog;

import java.util.Arrays;

public class HorseRace {
 
    public static void main(String[] args) {
        Horse test = new Horse("test", 3);
        Horse test2 = new Horse("test2", 3);
        HorseRace testRace = new HorseRace();
        BettingShop bs = new BettingShop();
        bs.open(testRace);
        var slip = bs.placeBet("test2", 500);
        System.out.println("premature payout: " + bs.payOut(slip));

        testRace.registerHorse(test);
        testRace.registerHorse(test2);
        testRace.start();
        System.out.println("valid slip: " + bs.validateBetSlip(slip));
        System.out.println("payout: " + bs.payOut(slip));
        System.out.println("payout: " + bs.payOut(slip));
        System.out.println(testRace.getChampion());
    }


    Horse[] registeredHorses = new Horse[0];
    Horse winner = null;

    public boolean registrateHorse(Horse horse) {
        return registerHorse(horse);
    }

    public boolean registerHorse(Horse horse) {
        if (horse == null) {
            return false;
        }
        boolean alreadyRegistered = false;

        for (int i = 0; i < registeredHorses.length && !alreadyRegistered; i++) {
            if (registeredHorses[i].getName().equals(horse.getName())) {
                alreadyRegistered = true;
            }
        }
        if (!alreadyRegistered) {
            registeredHorses = Arrays.copyOf(registeredHorses, registeredHorses.length + 1);
            registeredHorses[registeredHorses.length - 1] = horse;
        }
        return !alreadyRegistered;
    }

    public Horse[] listedHorses() {
        return registeredHorses;
    }

    public void start() {
        boolean finished = false;

        while (!finished) {
            for (Horse horse : registeredHorses) {
                horse.sprint();

                if (horse.getDistance() >= 100) {
                    finished = true;
                    if (winner == null || winner.getDistance() < horse.getDistance()) {
                        winner = horse;
                    }
                }
            }
        }
    }

    public String getChampion() {
        if (winner == null) {
            return null;
        } else {
            return winner.getName();
        }
    }
}

class Horse {
    private int maxVelocity;
    private String name;
    private int currentPosition = 0;

    public Horse(String name, int maxVelocity) {
        this.name = name;
        this.maxVelocity = maxVelocity;
    }

    public void sprint() {
        currentPosition += (Math.random()*maxVelocity) + 1;
    }

    public String getName() {
        return this.name;
    }

    public int getDistance() {
        return currentPosition;
    }
}

class BetSlip {
    private HorseRace race;
    private int bet;
    private String horseName;
    private double id;

    public double getId() {
        return id;
    }

    public HorseRace getRace() {
        return race;
    }

    public String getHorseName() {
        return horseName;
    }

    public int getBet() {
        return bet;
    }

    public BetSlip(HorseRace race, String horseName, int bet) {
        this.race = race;
        this.bet = bet;
        this.horseName = horseName;
        this.id = Math.random()*20+1;
    }
}

class BettingShop {

    private HorseRace race;
    private double[] payedOutSlips = new double[0];

    public void open(HorseRace race) {
        this.race = race;
    }

    public BetSlip placeBet(String horseName, int bet) {
        if (race != null){
            return new BetSlip(this.race, horseName, bet);
        } else {
            return null;
        }
    }

    public boolean validateBetSlip(BetSlip slip) {
        boolean validRace = slip.getRace() == race;

        Horse[] listedHorses = race.listedHorses();
        boolean validHorse = false;

        for (int i = 0; i < listedHorses.length && validRace && !validHorse; i++) {
            if (listedHorses[i].getName() == slip.getHorseName()) {
                validHorse = true;
            }
        }

        return validRace && validHorse;
    }

    public int payOut(BetSlip slip) {
        if (slip.getRace().winner != null) {
            boolean alreadyPaidOut = false;

            for (double payedOutSlip : payedOutSlips) {
                if (slip.getId() == payedOutSlip) {
                    alreadyPaidOut = true;
                }
            }

            payedOutSlips = Arrays.copyOf(payedOutSlips, payedOutSlips.length + 1);
            payedOutSlips[payedOutSlips.length - 1] = slip.getId();

            if (!alreadyPaidOut && slip.getRace().getChampion() == slip.getHorseName()) {
                return slip.getBet()*2;
            }
        } 
        return 0;
    }
}