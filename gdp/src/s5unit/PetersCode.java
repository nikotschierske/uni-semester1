package s5unit;

public class PetersCode {
	public static void main(String[] args) {
		 int j = 0, l = 0; double x = 0;
		 for (; x < 100; ++x,++l){
		 	 j++; //anstatt j = j++
		 }
		 for (; j > 0; --x,++l) { //x = Math.pow(x, 8)
		 	 if (x < x + 1) --j;
		 	 //Entweder x=x^8 entfernen oder das if entfernen, denn bei den Großen werten gerundet wird.
		 } 
		 System.out.println(j+" "+l);
	}
}
