package testat;

public class EggPack {

	public static void main(String[] args) {
		EggBox box = (new EggPack())
							.produce(10, new Hen());
		Egg[] removedEggs = box.get(6);
		box.shake();
//		System.out.println(box.toString());
	}
	
	public EggBox produce(int n, Hen hen) {
		EggBox box = new EggBox(n);
		for (int i = 0; i < n; i++) {
			box.put(hen.layEgg());
		}
		return box;
	}

}

class Egg {
	private int weight;
	private boolean broken;
	
	public Egg() {
		this.weight = (int) (Math.random()*10+1)+60;
		this.broken = false;
	}
	
	public void breakEgg() {
		if (Math.random()>0.5) {
			this.broken = true;
		}
	}
	
	public String toString() {
		if (broken) {
			return weight + " broken";
		} else {
			return String.valueOf(weight);
		}
	}
}

class Hen {
	public Egg layEgg() {
		return new Egg();
	}
}

class EggBox {
	private Egg[] eggs;
	private int eggsIterator;
	
	public EggBox(int capacity) {
		this.eggs = new Egg[capacity];
		this.eggsIterator = 0;
	}
	
	public boolean put(Egg egg) {
		if (this.eggsIterator != this.eggs.length) {
			this.eggs[this.eggsIterator] = egg;
			this.eggsIterator++;
			return true;
		} else {
			return false;
		}
	}
	
	public void shake() {
		for (int i = 0; i < this.eggs.length; i++) {
			if (this.eggs[i] != null) {
				this.eggs[i].breakEgg();
			}
		}
	}
	
	public Egg[] get(int n) {
		if (n != 0) {
			if (this.eggsIterator + 1 < n) {
				n = this.eggsIterator;
			}
			
			Egg[] removedEggs = new Egg[n];
			for (int i = 0; i < n; i++) {
				removedEggs[i] = this.eggs[this.eggsIterator - 1];
				this.eggsIterator -= 1;
			}
			
			return removedEggs;
		}
		return new Egg[0];
	}
	
	public String toString() {
		StringBuilder resultBuilder = new StringBuilder();
		for (int i = 0; i < this.eggsIterator; i++) {
			if (this.eggs[i] != null) {
				resultBuilder.append(this.eggs[i].toString()).append("|");
			} else {
				resultBuilder.append("empty | ");
			}
		}
		return resultBuilder.toString();
	}
}