package s0start;

import java.util.Scanner;

public class Num2Char {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String input = keyboard.next();
		if (input.length() == 6) {
			int[] cs = {
					Integer.parseInt(input.substring(0,2)),
					Integer.parseInt(input.substring(2,4)),
					Integer.parseInt(input.substring(4,6))
			};
			
			StringBuilder resultBuilder = new StringBuilder();
			for (int i = 0; i < cs.length; i++) {
				resultBuilder.append((char)cs[i]);
			}
			System.out.println("decode = " + resultBuilder.toString());
			keyboard.close();
		}
	}
}
