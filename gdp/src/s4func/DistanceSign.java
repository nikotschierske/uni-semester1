package s4func;

import java.util.Scanner;

public class DistanceSign {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		float lengthInput = scanner.nextFloat();
		String einheit = scanner.next();
		
		
		if (!(einheit != "mm" || einheit != "km" || einheit != "m")) {
			System.out.println("Unbekannte Einheit");
		} else {
			if (lengthInput<0) { lengthInput = -lengthInput; }
			
			int lang = 0;
			switch(einheit) {
				case "mm":
					lang = (int) lengthInput;
					break;
				case "m":
					lang = (int) (lengthInput * 1000);
					break;
				case "km":
					lang = (int) (lengthInput * 1000000);
					break;
			}
			System.out.println("lang = " + lang + " mm");
			
			int langGanzzahlig = lang;
			String langEinheit = " mm";
			
			if(lang%1000000 == 0) {
				langGanzzahlig = lang/1000000;
				langEinheit = " km";
			} else if (lang%1000 == 0) {
				langGanzzahlig = lang/1000;
				langEinheit = " m";
			} 
			System.out.println("kurz = " + langGanzzahlig + langEinheit);
			int stellen = 0;
			int langGanzzahligBK = langGanzzahlig;
			while(langGanzzahlig != 0) {
				langGanzzahlig/=10;
				stellen++;
			}
			if (langEinheit == " m") { stellen += 2;} else { stellen += 3; }
			System.out.println("stellen = " + stellen);
			for (int i = 0; i < stellen + 4; i++) {
				System.out.print("*");
			}
			System.out.println();
			System.out.println("* " + langGanzzahligBK + langEinheit +  " *");
			for (int i = 0; i < stellen + 4; i++) {
				System.out.print("*");
			}
			System.out.println();
			for (int i = 0; i < stellen/2; i++) {
				for (int j = 0; j < stellen/2 + 2; j++) {
					System.out.print(" ");
				}
				System.out.println("*");
			}
			
		}
		scanner.close();
	}
}
