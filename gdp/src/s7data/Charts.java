package s7data;

public class Charts {
	public static void main(String[] args) {
		Song[] charts = new Song[10];
		Artist ginger = new Artist("Ed Sheeran", "UK");
		Artist johnLennonAndSmaug = new Artist("Imagine Dragons", "USA");
		charts[0] = new Song("Shape of You", 1, 89, ginger);
		charts[3] = new Song("Thunder", 2, 50, johnLennonAndSmaug);
		charts[9] = new Song("Galway Girl", 5, 48, ginger);
		System.out.println();
	}
}

class Song {
	Song(String title, int bestPlace, int chartWeeks, Artist artist) {
		this.title = title;
		this.bestPlace = bestPlace;
		this.chartWeeks = chartWeeks;
		this.artist = artist;
	}
	
	String title;
	int bestPlace;
	int chartWeeks;
	Artist artist;
}

class Artist {
	Artist(String name, String country){
		this.name = name;
		this.country = country;
	}
	String name;
	String country;
}