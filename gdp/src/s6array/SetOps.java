package s6array;

public class SetOps {
	public static void main(String[] args) {
		char[] a = {'a','b','c'};
		char[] b = {'a','b'};
		char[] c = {}; 
		System.out.println(isSubsetOf(b, a));
		System.out.println(isSubsetOf(c, a));
	} 
	  
	
	public static boolean isSubsetOf(char[] a, char[] b) {
		boolean subset = true;
		if (b.length == 0 && a.length != 0) { subset=false; }
		for (int i = 0; i < a.length; i++) {
			boolean ijEqual = false;
			for (int j = 0; j < b.length; j++) {
				if (a[i]==b[j]) { ijEqual = true; } 
			}
			subset = subset && ijEqual;
		}
		return subset;
	} 
	
	public static boolean isEqualTo(char[] a, char[] b) {
		return (isSubsetOf(a,b) && isSubsetOf(b,a));
	}
}
