package s4func;

public class LongestWord {
	public static String pickLongest(String text) {
		String currentLongest = "";
		String currentWord = "";
		for (int i = 0; i < text.length(); i++) {
			char currentChar = text.charAt(i);
			if (currentChar == ' ') {
				if (currentWord.length() > currentLongest.length()) {
					currentLongest = currentWord;
				}
				currentWord = "";
			} else {
				currentWord = currentWord + currentChar;
			} 
		}
		if (currentWord.length() > currentLongest.length()) {
			currentLongest = currentWord;
		}
		return currentLongest;
	}
}