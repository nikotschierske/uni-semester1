package s0start;

import java.util.Scanner;

public class TentCanvas {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		//Base Values
		double tentWidth = scanner.nextDouble() * 100;
		double tentDepth = scanner.nextDouble() * 100;
		
		double tentBevel = tentWidth / (2 * Math.sin(Math.toRadians(90)/2));
		scanner.close();
		
		//Solo Area calculation
		double tentSidesTotal = 2 * tentBevel * tentDepth; //Ceiling bevels of the tent
		double tentBase = tentDepth * tentWidth; //Floor
		double tentTriangleTotal = tentBevel * tentBevel; //Triangle Sides of the tent
		
		//Total Area calculation
		double totalArea = tentSidesTotal + tentBase + tentTriangleTotal;
		System.out.println("qcm = " + (int)totalArea);
		
	}
}
