package s5unit;

import org.junit.Test;
import junit.framework.Assert;

public class Power {

	/**
	 * Returns the value of the bases raised to the power of the exponent. If the
	 * exponent is 0, this returns 1 If the base is 0, this returns
	 * 
	 * @param x base
	 * @param n exponent
	 * @return value of x raised to the power of n
	 **/ 
	public static double power(double x, int n) {
		if (n == 0) { 
			return 1.0;
		}
		double z = x;
		while (n > 1) {
			z *= x;
			n -= 1;
		}
		while (n < 1) {
			z /= x;
			n += 1;
		}
		return z;
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void powerTest0() {
		for (int i = 1; i < 10; i++) {
			Assert.assertEquals(0.0, power(0.0, i), 0.01);
		}
	}
	@SuppressWarnings("deprecation")
	@Test
	public void powerTest1() {
		for (int i = 0; i < 10; i++) {
			Assert.assertEquals(1.0, power(1.0, i), 0.01);
		}
	}
	@SuppressWarnings("deprecation")
	@Test
	public void powerTest20() {
		Assert.assertEquals(1.0, power(2.0, 0), 0.01);
	}
	@SuppressWarnings("deprecation")
	@Test
	public void powerTest21() {
		Assert.assertEquals(2.0, power(2.0, 1), 0.01);
	}
	@SuppressWarnings("deprecation")
	@Test
	public void powerTest22() {
		Assert.assertEquals(4.0, power(2.0, 2), 0.01);
	}
	@SuppressWarnings("deprecation")
	@Test
	public void powerTest35() {
		Assert.assertEquals(243.0, power(3.0, 5), 0.01);
	}
	@SuppressWarnings("deprecation")
	@Test
	public void powerTest155() {
		Assert.assertEquals(759375.0, power(15.0, 5), 0.01);
	}
	@SuppressWarnings("deprecation")
	@Test
	public void powerTestNegatives() {
		Assert.assertEquals(0.25, power(2.0, -2), 0.01);
	}
}
