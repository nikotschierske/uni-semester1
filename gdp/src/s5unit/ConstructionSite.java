package s5unit;

import java.util.Scanner;

import std.ui.MiniApp;

public class ConstructionSite extends MiniApp {

	public static int bricks = 0;
	public static int material = 0;
		
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int p = scanner.nextInt();
		int w = scanner.nextInt();
		int f = scanner.nextInt();
		buildFloor(p,w,f); 
		buildFloor(p,w,f);
		buildFloor(p,w,f);
		scanner.close();
	}
	
	public static void buildWall(int p, int w, int h) {
		for (int i = 0; i < h; i++) {
			buildCustomSizedBricks(p, w);
			material+=w;
		}
	}
	
	public static void buildFloor(int p, int w, int f) {
		buildWall(p,w,2);
		buildWall(p,f,3);
		buildWall(p+f+5,w-f-5,3);
		buildWall(p,w,1);
	}
	
	public static void buildCustomSizedBricks(int position, int width) {
		int currentPos = position;
		//Allowed Bricks: 8,6,4,3,2,1
		if (width/8 > 0) {
			for (int i = 0; i < width/8; i++) {
				placeBrick(currentPos, true, 8);
				currentPos += 8; bricks++;
			}
			width %= 8;
		}
		if (width/6 == 1) {
			placeBrick(currentPos, true, 6);
			currentPos += 6; bricks++;
			width %= 6;
		}
		if (width/4 == 1) {
			placeBrick(currentPos, true, 4);
			currentPos += 4; bricks++;
			width %= 4;
		}
		if (width/3 == 1) {
			placeBrick(currentPos, true, 3);
			currentPos += 3; bricks++;
			width %= 3;
		}
		if (width/2 == 1) {
			placeBrick(currentPos, true, 2);
			currentPos += 2; bricks++;
			width %= 2;
		}
		if (width == 1) {
			placeBrick(currentPos, true, 1);
			bricks++;
		}
	}

}
