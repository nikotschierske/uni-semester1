package s6array;

public class MostFrequent {

	public static void main(String[] args) {
		int[] b = {1,1,2,3};
		System.out.println(mostfreq(b));
	}
	
	/**
	 * Returns most frequent number from an array of ints.
	 * Returns -1 if the given array is empty.
	 * 
	 * @param 	a Array
	 * @return 	most frequent number from a
	 */ 
	public static int mostfreq(int[] a) {
		int currentOut = -1;
		int counter = -1; //CurrentMostFrequentFrequency
		for (int i = 0; i < a.length; i++) {
			int frequencyI = -1;
			for (int j = 0; j < a.length; j++) {
				if (a[j] == a[i]) {
					frequencyI++;
				}
			}
			if (frequencyI > counter) {
				currentOut = a[i];
				counter = frequencyI;
			}
		}
		return currentOut;
	}

}
