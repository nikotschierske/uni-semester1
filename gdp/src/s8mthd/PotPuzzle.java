package s8mthd;

import java.awt.Color;

import std.ui.MiniApp;

public class PotPuzzle extends MiniApp {

    public static void main(String[] args) throws Exception {
        Pot pot = new Pot(50, 3000, 0);
        Pot pot2 = new Pot(150, 5000, 0);
        Pot pot3 = new Pot(250, 8000, 8000);

        Pot[] potArray = new Pot[] { null, pot, pot2, pot3 };
        for (Pot potI : potArray) {
            if (potI != null) {
                potI.show();
            } 
        }

        char key1Char = NO_KEY;
        char key2Char = NO_KEY;

        while (key1Char != 'e' && key2Char != 'e') {
            if (isValidInput(potArray, key1Char) && isValidInput(potArray, key2Char)){
                String key1String = String.valueOf(key1Char);
                String key2String = String.valueOf(key2Char);

                int pourFrom = Integer.valueOf(key1String);
                int pourTo = Integer.valueOf(key2String);

                System.out.println("Pouring");
                potArray[pourFrom].pourInto(potArray[pourTo]);

                for (Pot potI : potArray) {
                    if (potI != null) {
                        potI.show();
                    }
                }
            }

            key1Char = getRealKey();
            System.out.println(key1Char);

            key2Char = getRealKey();
            System.out.println(key2Char);
        }
        MiniApp.shutdown();
    }

    private static boolean isValidInput(Pot[] potArray, char testChar) {
        return ((int) testChar > 48 && ((int) testChar < 49 + potArray.length - 1));
    }

    private static char getRealKey() {
        char key = NO_KEY;
        while (key == NO_KEY) {
            key = getKey();
        }
        return key;
    }
}

class Pot extends MiniApp {
    private int x;
    private int maxCapacity;
    private int currentFillStatus;

    public Pot(int x, int max, int cur) {
        this.x = x;
        this.maxCapacity = max;
        if (cur <= this.maxCapacity) {
            this.currentFillStatus = cur;
        } else {
            this.currentFillStatus = this.maxCapacity;
        }
    }

    public int getCurrent() {
        return currentFillStatus;
    }

    public int getMaximum() {
        return maxCapacity;
    }

    public void fill() {
        this.currentFillStatus = this.maxCapacity;
    }

    public void fill(int amount) {
        this.currentFillStatus += amount;
    }

    public int getX() {
        return this.x;
    }

    public void pourInto(Pot a) {
        if (a != this && this.x != a.getX()) {
            int aCapacity = (a.getMaximum() - a.getCurrent());
            if (aCapacity < this.currentFillStatus) {
                this.currentFillStatus -= aCapacity;
                a.fill();
            } else {
                a.fill(this.currentFillStatus);
                this.currentFillStatus = 0;
            }
        }
    }

    public void show() {
        int width = 50;
        int xPos = this.x - width / 2;

        Color potColor = Color.BLACK;
        Color fillColor = Color.BLUE;
        if (this.currentFillStatus == 1000) {
            potColor = Color.GREEN;
        }

        int potHeight = this.maxCapacity / width;
        int fillHeight = this.currentFillStatus / width;

        MiniApp.drawRectangle("pot" + this.x, xPos, 200 - potHeight, width, potHeight, potColor);
        MiniApp.fillRectangle("fill" + this.x, xPos, 200 - fillHeight, width, fillHeight, fillColor);
    }
}