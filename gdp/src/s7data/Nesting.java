package s7data;

public class Nesting {
	public static void main(String[] args) {
		System.out.println(describe(new Matryoshka("a", 5, new Matryoshka("b", 2, null))));
		System.out.println(getTotalWeightR(new Matryoshka("a", 5, new Matryoshka("b", 2, null))));
		System.out.println(getTotalWeightI(new Matryoshka("a", 5, new Matryoshka("b", 2, null))));
	}

	static Matryoshka nest(String n, int w, Matryoshka m) {
		return new Matryoshka(n, w, m);
	}

	static Matryoshka unnest(Matryoshka m) {
		return m.nestedMatryoshka;
	}

	static String describe(Matryoshka m) {
		if (m == null) {
			return "";
		}
		if (m.nestedMatryoshka == null) {
			return "[" + m.name + " " + m.weight + "]";
		}
		return "[" + m.name + " " + describe(m.nestedMatryoshka) + " " + m.weight + "]";
	}

	static int getTotalWeightR(Matryoshka m) {
		if (m.nestedMatryoshka == null) {
			return m.weight;
		}
		return m.weight + getTotalWeightR(m.nestedMatryoshka);
	}

	static int getTotalWeightI(Matryoshka m) {
		Matryoshka currentM = m;
		int totalWeight = 0;

		while (currentM != null) {
			totalWeight += currentM.weight;
			currentM = currentM.nestedMatryoshka;
		}
		return totalWeight;
	}
}

class Matryoshka {
	String name;
	int weight;
	Matryoshka nestedMatryoshka;

	public Matryoshka(String name, int weight, Matryoshka nestedMatryoshka) {
		this.name = name;
		this.weight = weight;
		this.nestedMatryoshka = nestedMatryoshka;
	}
}