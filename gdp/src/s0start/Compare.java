package s0start;

import java.util.Scanner;

public class Compare {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int x = scanner.nextInt(); int y = scanner.nextInt();
		
		int compare = (x < y) ? -1 : (x>y) ? 1 : 0;
		
		System.out.println("compare = " + Integer.toString(compare));
		
		scanner.close();
	}
}
