package s0start;

import java.awt.Color;
import java.util.Scanner;

import std.ui.MiniApp;

public class FirstIf extends MiniApp {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int inputN = scanner.nextInt(); //Getting Number input
		drawText("text" , 50, 50, Integer.toString(inputN), Color.blue); 
		Color circleColor;
		if ((inputN % 2) == 1)  //if inputN is uneven
			circleColor = Color.red;
		else 
			circleColor = Color.green;
		fillCircle("gerade", 50, 100, 10, circleColor); //First Circle, green if even
		circleColor = (inputN > 10) ? Color.green : Color.red;
		fillCircle("gross", 50, 150, 10, circleColor); //Second Circle, green if > 10
		scanner.close();
	}
}
