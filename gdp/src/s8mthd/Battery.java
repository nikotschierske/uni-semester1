package s8mthd;

public class Battery {
	private int maxCapacity;
	private int currentCapacity;
	private int cycles;

	public static void main(String[] args) {
		Battery battery = new Battery(1000);
		while (battery.consume() != "") {
			battery.load(1000);
			battery.consume();
		}
	}

	public Battery(int maxCapacity) {
		this.maxCapacity = maxCapacity;
		this.currentCapacity = 0;
		this.cycles = 0;
	}
	
	public int getCapacity() {
		return currentCapacity;
	}
	
	public boolean isFull() {
		return (this.currentCapacity >= (this.maxCapacity * 0.9));
	}
	
	public void load(int mAh) {
		this.currentCapacity += mAh;
		if (this.currentCapacity > this.maxCapacity) {
			this.currentCapacity = this.maxCapacity;
		} 
		this.cycles++;
		this.maxCapacity--; 
	}
	
	public String consume() {
		int starN = this.currentCapacity/20;
		String returnString = "";
		for (int i = 0; i < starN; i++) {
			returnString += '*';
		}
		this.currentCapacity = 0;
		return returnString;
	}
}
