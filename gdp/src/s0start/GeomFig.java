package s0start;

import java.awt.Color;
import java.util.Scanner;

import std.ui.MiniApp;

public class GeomFig extends MiniApp {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int pxArea = scanner.nextInt();
		
		//Kreis 
		int circleRadius = (int) Math.sqrt(pxArea/Math.PI); //WISSEN OB MATH.PI zugelassen ist, bevor man es benutzt
		fillCircle("Kreis", 75, 100, circleRadius, Color.orange);
		
		//Quadrat
		int squareLen = (int) Math.sqrt(pxArea);
		fillRectangle("Quadrat", 150-(squareLen/2), 100-(squareLen/2), squareLen, squareLen, Color.blue);
		
		//Platte mit Bohrung
		int panelLen = (int) Math.sqrt((16*pxArea)/(16-Math.PI));
		int holeRadius = (int)(panelLen/4);
		fillRectangle("Platte", 225-(panelLen/2), 100-(panelLen/2), panelLen, panelLen, Color.magenta);
		fillCircle("Bohrung", 225, 100, holeRadius, Color.white);
		
		scanner.close(); // ans Ende, damit man leichter später noch einen Scan einbauen kann
	}
}
