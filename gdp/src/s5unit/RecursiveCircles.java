package s5unit;

import java.awt.Color;

import std.ui.MiniApp;

public class RecursiveCircles extends MiniApp  {
	
	public static int noOfCircles = 1;

	public static void main(String[] args) {
		drawCircles(500, 500, 500, 1);
	}
	
	public static void drawCircles(double x, double y, double radius, double minimumRadius) {
		if (radius >= minimumRadius) {
			double newRadius = radius / (Math.sqrt(2) + 1);
			drawCircle("circle" + noOfCircles, (int) x, (int) y, (int) radius, Color.black);
			noOfCircles++;
			drawCircles(x-newRadius, y-newRadius, newRadius, minimumRadius);
			drawCircles(x+newRadius, y+newRadius, newRadius, minimumRadius);
			drawCircles(x-newRadius, y+newRadius, newRadius, minimumRadius);
			drawCircles(x+newRadius, y-newRadius, newRadius, minimumRadius);
		}
	}
	
	public static int getNoOfCircles() {
		return noOfCircles;
	}
}
