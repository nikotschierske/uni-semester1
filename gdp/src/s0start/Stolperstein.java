 package s0start;
 
 import std.ui.MiniApp;
 import java.awt.Color;
import java.util.Iterator;
import java.util.Scanner;
 
// public class Stolperstein extends MiniApp {
// 	public static void main(String[] args) {
// 		Scanner keyboard = new Scanner(System.in);
// 		int maximaleBreite = 30;
// 		int breite = keyboard.nextInt();
// 		int abstand = (maximaleBreite - breite*3 ) / 4;
// 		int currentPosition = abstand;
// 		
// 		placeBrick(currentPosition, true, breite, Color.BLUE);
// 		currentPosition += abstand + breite;
// 		placeBrick(currentPosition, true, breite, Color.BLUE);
// 		currentPosition += abstand + breite;
// 		placeBrick(currentPosition, true, breite, Color.BLUE);
// 		
// 		keyboard.close();
// 	}
// }

public class Stolperstein extends MiniApp {
 	public static void main(String[] args) {
 		Scanner keyboard = new Scanner(System.in);
 		int MAXIMALE_BREITE = 30;
 		int ANZAHL_STEINE = 3;
 		int breite = keyboard.nextInt();
 		int abstand = (MAXIMALE_BREITE - breite*ANZAHL_STEINE ) / (ANZAHL_STEINE + 1);
 		int currentPosition = abstand;
 		
 		for (int i = 0; i < ANZAHL_STEINE; i++) {
 			System.out.println(i);
 			placeBrick(currentPosition, true, breite, Color.BLUE);
 	 		currentPosition += abstand + breite;
		}
 		
 		keyboard.close();
 	}
 }