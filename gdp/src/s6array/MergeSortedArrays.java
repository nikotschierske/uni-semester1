package s6array;

import java.util.Arrays;

public class MergeSortedArrays {

	public static void main(String[] args) {
		int[] a = { 1, 3, 5 };
		int[] b = { 2, 4 };
		System.out.println(Arrays.toString(merge(a, b)));
	} 

	/**
	 * Merges two sorted integer arrays
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static int[] merge(int[] a, int[] b) {
		if (a.length == 0) {
			return b;
		} else if (b.length == 0) {
			return a;
		}
 
		int[] sortedMerged = new int[a.length + b.length];

		int ai = 0;
		int bi = 0;
		for (int i = 0; i < sortedMerged.length; i++) {
			if (bi == b.length) {
				sortedMerged[i] = a[ai];
				ai++;
			} else if (ai == a.length) {
				sortedMerged[i] = b[bi];
				bi++;
			} else {
				if (a[ai] > b[bi]) {
					sortedMerged[i] = b[bi];
					bi++;
				} else {
					sortedMerged[i] = a[ai];
					ai++;
				}
			}
		}

		return sortedMerged;
	}
}
