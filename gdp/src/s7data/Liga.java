package s7data;

import static org.junit.Assert.*;

public class Liga {
	public static Team[] teams;
	
	public static void main(String[] args) {
		createTeams("A", "B", "C");
		match(teams[0], teams[1], 3, 1);
		System.out.println(teams[0].goals);
		System.out.println(teams[0].matches);
		System.out.println(getTeam("B").points);
		match(teams[2], teams[1], 2, 2);
		System.out.println(getTeam("B").points);
		System.out.println(getTeam("B").matches);
		Team t = new Team("Z", 0, 0, 0);
		match(t, teams[2], 1, 7);
		System.out.println(t.points);
	}
	
	static Team[] createTeams(String... name) {
		assertNotNull(name);
		assertTrue(name.length > 0);
		Team[] newTeams = new Team[name.length];
		for (int i = 0; i < name.length; i++) {
			newTeams[i] = new Team(name[i], 0, 0, 0);
		}
		teams = newTeams;
		return newTeams;
	} 
	
	static Team getTeam(String name) {
		assertNotNull(name);
		Team team = null;
		for (int i = 0; i < teams.length; i++) {
			if (teams[i].name.equals(name)) {
				team = teams[i];
			}
		}
		return team;
	}
	
	static void match(Team heim, Team gast, int toreheim, int toregast) {	
		assertNotNull(heim);
		assertNotNull(gast);
		assertNotEquals(heim, gast);
		assertTrue(toregast >= 0 && toreheim >= 0);

		
		heim.goals += toreheim - toregast;
		gast.goals += toregast - toreheim;
		heim.matches++;
		gast.matches++;
		if (toreheim > toregast) {
			heim.points += 3;
		} else if (toregast > toreheim) {
			gast.points += 3;
		} else { 				//Unentschieden
			heim.points++;
			gast.points++;
		}
	}
	
	static int getGoalDiff(Team team) {
		assertNotNull(team);
		return team.goals;
	}
	
	static int getMatches(Team team) {
		assertNotNull(team);
		return team.matches;
	}
	static int getPoints(Team team) {
		assertNotNull(team);
		return team.points;
	}
}

class Team {
	String name;
	int goals;
	int matches;
	int points;
	
	public Team(String name, int goals, int matches, int points) {
		this.name = name;
		this.goals = goals;
		this.matches = matches;
		this.points = points;
	}
}