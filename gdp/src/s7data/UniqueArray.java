package s7data;

public class UniqueArray {
	public static int unique(int[] inputArray) {
		int counter = 0;
		for (int i = 0; i < inputArray.length; i++) {
			boolean ijEquals = false;
			for (int j = 0; j < inputArray.length && !ijEquals; j++) {
				if (inputArray[i] == inputArray[j] && i != j) {
					ijEquals = true;
				}
			}
			if (!ijEquals) {
				counter++;
			}
		}
		return counter;
	}
}