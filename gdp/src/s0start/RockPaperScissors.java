package s0start;

import java.util.Scanner;

public class RockPaperScissors {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char playerAChar = scanner.next().charAt(0);
		char playerBChar = scanner.next().charAt(0);
		System.out.println(getDecision(playerAChar, playerBChar));
		
		scanner.close();
	}
	
	public static boolean schummelt(char testing) {
		return !((testing == 'r') || (testing == 'p') || (testing == 's'));
	}
	
	public static String getDecision(char A, char B) {
		boolean aSchummelt = schummelt(A);
		boolean bSchummelt = schummelt(B);
		String decision = 
				bSchummelt ? 
						(!aSchummelt ? "B schummelt" : "Beide schummeln") :
							(aSchummelt ? "A schummelt" : "");
		
		if (!(decision == ""))
			return decision;
		
		if (A == B)
			return "Unentschieden";
		
		switch (A) {
			case 'r':
				if (B == 'p')
					return "B gewinnt";
				else 
					return "A gewinnt";
			case 'p':
				if (B == 's')
					return "B gewinnt";
				else 
					return "A gewinnt";
			case 's':
				if (B == 'r')
					return "B gewinnt";
				else 
					return "A gewinnt";
			default:
				return "";
		}
	}
}
