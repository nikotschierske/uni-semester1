package sAprog;

import java.util.Arrays;

public class PolynomTable {
    public static void main(String[] args) {
        Polynom p1 = new Polynom(0.5,3,2,1,0);
        Polynom p2 = p1.derivative();
        System.out.println(p2.toString());
    }
}

class Polynom {
    public int maxFactor;
    public double[] coefficients;

    public Polynom(double... coefficients) {
        this.coefficients = coefficients;
        this.maxFactor = coefficients.length - 1;
    }

    public Polynom derivative() {
        if (coefficients.length == 0) {
            return this;
        } else {
            Polynom derivative = new Polynom(
                Arrays.copyOf(coefficients, coefficients.length - 1)
            );
            return derivative;
        }
    }

    public int getMaxFactor() {
        return maxFactor;
    }

    public double[] getCoefficients() {
        return coefficients;
    }


    public boolean equals(Polynom equalTo) {
        double[] longerArray = this.getCoefficients();
        double[] shorterArray = equalTo.getCoefficients();
        boolean equalLength = false;

        if (shorterArray.length > longerArray.length) {
            longerArray = equalTo.getCoefficients();
            shorterArray = this.getCoefficients();
        } else {
            equalLength = (shorterArray.length == longerArray.length);
        }

        if (!equalLength) {
            for (int i = 0; i < longerArray.length - shorterArray.length; i++) {
                if (longerArray[i] != 0) {
                    return false;
                }
            }
        }

        return equalWithEqualLengths(longerArray, shorterArray);
    }

    private boolean equalWithEqualLengths(double[] longer, double[] shorter) {
        boolean equals = true;
        int longI = longer.length - shorter.length;

        for (int shortI = 0; shortI < shorter.length; shortI++, longI++) {
            if (shorter[shortI] != longer[longI]) {
                equals = false;
            } 
        }
        return equals;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.coefficients.length - 1; i++) {
            stringBuilder.append(this.coefficients[i])
                .append("x^")
                .append(this.maxFactor - i)
                .append(" + ");
        }
        stringBuilder.append(this.coefficients[this.coefficients.length - 1]);
        return stringBuilder.toString();
    }
}

class Table {
    Polynom polynom;
    int from;
    int to;
    int steps;

    public Table(Polynom polynom, int from, int to, int steps) {
        this.from = from;
        this.to = to;
        this.steps = steps;
        this.polynom = polynom;
    }

    public double getX(double x) {
        double diff = Math.abs(this.to - this.from);
        double stepSize = diff / (steps - 1);
        return this.from + stepSize * x;
    }


    public double getY(double x) {
        x = getX(x);
        System.out.println("x = " + x);

        double y = 0;

        for (int i = 0; i < this.polynom.coefficients.length; i++) {
            int degree = this.polynom.maxFactor - i;
            System.out.println("Degree = " + degree);
            y += this.polynom.coefficients[i] * Math.pow(x, degree);
        }

        return y;
    }
}
