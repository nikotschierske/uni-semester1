package s4func;

public class LooterLang {
	public static String encode(String text) {
		String encodedText = "";
		boolean wasSpace = true;
		for (int i = 0; i < text.length(); i++) {
			char currentChar = text.toLowerCase().charAt(i);
			if (wasSpace) {
				if (Character.isUpperCase(text.charAt(i))){
					encodedText = encodedText + Character.toUpperCase(currentChar);
				}
				else {
					encodedText = encodedText + currentChar;
				}
			}
			else {
				encodedText = encodedText + currentChar;
			}
			if (isConsonant(currentChar)) {
				encodedText = encodedText + 'o' + currentChar;
			}
			wasSpace = (currentChar == ' ');
		}
		return encodedText;
	}
	
	public static boolean isConsonant(char testChar) {
		return (testChar == 'j') || (testChar == 'b') || (testChar == 'c') || (testChar == 'd') || (testChar == 'f') || (testChar == 'g') || (testChar == 'h') || (testChar == 'k') || (testChar == 'l') || (testChar == 'm') || (testChar == 'n') || (testChar == 'p') || (testChar == 'q') || (testChar == 'r') || (testChar == 's') || (testChar == 't') || (testChar == 'v') || (testChar == 'w') || (testChar == 'x') || (testChar == 'y') || (testChar == 'z');
	}
	
	public static String decode(String text) {
		String decodedText = "";
		for (int i = 0; i < text.length(); i++) {
			char currentChar = text.charAt(i);
			char currentCharLower = text.toLowerCase().charAt(i);
			decodedText = decodedText + currentChar;
			if (isConsonant(currentCharLower)) {
				i += 2;
			}
		}
		return decodedText;
	}
	
	public static void main(String[] args) {
		String input = "Kalle Blomquist";
		System.out.println(encode(input));
		System.out.println(decode(encode(input)));
	}
}
