package s6array;

import java.util.Arrays;

public class LetterArray {

	public static void main(String[] args) {
		System.out.println(Arrays.toString(countLetters("abcdefghijklmnopqrstuvwxyzaaaaaa12341946213789()()()()()()()sazdgZZZSADHSDBJH")));
	}
	
	public static int[] countLetters(String text) {
		int[] letterCount = new int[26];
		
		for (int i = 0; i < text.length(); i++) {
			int charAscii = (int) text.toLowerCase().charAt(i);
			
			if (charAscii <= 'z' && charAscii >= 'a') {
				letterCount[charAscii- 'a']++;
			}
		}
		return letterCount;
	}
}
