package s0start;

import java.awt.Color;

import std.ui.MiniApp;

public class FirstLoop extends MiniApp {
	
	static int START_POS_X = 50;
	static int START_POS_Y = 0;
	static int START_POS_Y_END = 200;
	static int STEPS_X = 2;
	static int STEPS_Y = 2;
			
	public static void main(String[] args) {		
		for (int j = 0; j < 50; j++) {
			int posX = START_POS_X + (j * STEPS_X);
			int posY = START_POS_Y + (j * STEPS_Y);
			int posYEnd = START_POS_Y_END - (j*STEPS_Y);
			drawLine("linie" + posX, posX, posY, posX, posYEnd, Color.red);
		}
		for (int j = 0; j < 50; j++) {
			int posX = START_POS_X + 100 + (j * STEPS_X);
			int posY = START_POS_Y + 100 - (j * STEPS_Y);
			int posYEnd = START_POS_Y_END - 100 + (j*STEPS_Y);
			drawLine("linie" + posX, posX, posY, posX, posYEnd, Color.red);
		}
	}
}
